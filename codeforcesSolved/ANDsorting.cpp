
#include <cmath>
#include <vector>
#include <cassert>
#include <iostream>
using namespace std;
typedef long long lint; 


int main() {
  lint nc;
  cin >> nc;
  for(lint i = 0; i < nc;i++){
    lint n;
    cin >> n;
    lint array[n];
    for( lint j =0  ; j < n; j++){
      cin >> array[j];
    }
    lint out = -1;
    for( lint j =0  ; j < n; j++){
      if(array[j]!=j){
        out &= array[j];
      }
    }
    cout << out << endl;
  }
}

