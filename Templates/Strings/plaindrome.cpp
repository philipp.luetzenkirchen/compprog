#include <bits/stdc++.h>

using namespace std;

//manacher algorithm implementation in O(n)
string longest_plalindrome_manacher(const string& S){
    int n = S.size();
    string T = "^#";
    for(int i = 0; i < n; i++){
        T += S[i];
        T += "#";
    }
    T += "$";
    int nt = T.size();
    vector<int> P(nt, 0);
    int C = 0, R = 0;
    for(int i = 1; i < nt - 1; i++){
        int i_mirror = 2 * C - i;
        if(R > i){
            P[i] = min(R - i, P[i_mirror]);
        }
        while(T[i + 1 + P[i]] == T[i - 1 - P[i]]){
            P[i]++;
        }
        if(i + P[i] > R){
            C = i;
            R = i + P[i];
        }
    }
    int max_len = 0;
    int center_index = 0;
    for(int i = 1; i < nt - 1; i++){
        if(P[i] > max_len){
            max_len = P[i];
            center_index = i;
        }
    }
    int start = (center_index - max_len) / 2;
    return S.substr(start, max_len);
}

int main(){
    string s = "abcccba" ; 
    cout << longest_plalindrome_manacher(s) << endl;
}