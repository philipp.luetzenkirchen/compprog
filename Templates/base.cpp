#include <bits/stdc++.h>

using namespace std;

string to_string(const char* str){
    return string(str);
}

string to_string(bool b){
    return b ? "true" : "false";
}

template<typename A, typename B>
string to_string(const std::pair<A, B>& p){
    return "(" + to_string(p.first) + ", " + to_string(p.second) + ")";
}

template<typename A>
string to_string(const A& v){
  bool first = true ; 
  string res = "{";
  for(const auto& x : v){
    if(!first) res += ", ";
    first = false;
    res += to_string(x);
  }
  res += "}";
  return res;
}

void debug_out() { cerr << endl; }

template<typename Head, typename... Tail>
void debug_out(Head H, Tail... T) {
  cerr << " " << to_string(H);
  debug_out(T...);
}

#ifdef LOCAL
#define debug(...) cerr << "[" << #__VA_ARGS__ << "]:", debug_out(__VA_ARGS__)
#else
#define debug(...) 
#endif


void solve(int test_case) {
  debug(test_case);
  
}

int main() {
  ios_base::sync_with_stdio(false) ;
  cin.tie(0) ;
  int t;
  cin >> t;

  while (t--) {
    solve(t);
  }

  return 0;
}