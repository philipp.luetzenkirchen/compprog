#include <bits/stdc++.h>

using namespace std;

template <typename T_weight,
          enable_if<is_integral<T_weight>::value, bool> = true>
struct dijkstra {
  const T_weight inf = numeric_limits<T_weight>::max() / 2;

  struct edge {
    int node = -1;
    T_weight weight = 0;

    edge() {}

    edge(int _node, T_weight _weight) : node(_node), weight(_weight) {}
  };

  struct state {
    T_weight dist;
    int node;

    state() {}

    state(T_weight _dist, int _node) : dist(_dist), node(_node) {}

    bool operator<(const state &rhs) const { return dist > rhs.dist; }
  };

  int n;
  vector<vector<edge>> adj;
  vector<T_weight> dist;
  vector<int> parent;

  dijkstra(int _n) {
    n = _n;
    adj.assign(n, {});
  }

  void add_directional_edge(int a, int b, T_weight weight) {
    adj[a].push_back(edge(b, weight));
  }

  void dijkstra_check(priority_queue<state> &pq, int node, int from,
                      T_weight new_dist) {
    if (new_dist < dist[node]) {
      dist[node] = new_dist;
      parent[node] = from;
      pq.emplace(new_dist, node);
    }
  }

  void dijkstra_solve(const vector<int> &source) {
    if (n == 0)
      return;
    dist.assign(n, inf);
    parent.assign(n, -1);
    priority_queue<state> pq;

    for (auto s : source) {
      dijkstra_check(pq, s, -1, 0);
    }

    while (!pq.empty()) {
      state top = pq.top();
      pq.pop();

      if (top.dist > dist[top.node]) {
        continue;
      }

      for (auto &e : adj[top.node]) {
        dijkstra_check(pq, e.node, top.node, top.dist + e.weight);
      }
    }
  }
};

int main() { 
    return 0 ;
 }