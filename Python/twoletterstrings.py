#!/usr/bin/env python3


import random
import sys


def doTest(l,strings):
    sum = 0
    #print(strings)
    for string in strings:
        for string2 in strings:
            if string == string2:
                continue
            #print(string,string2)
            if string[0] == string2[0] or string[1] == string2[1]:
                sum += strings[string]*strings[string2]
        
    print(int(sum/2))
import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        if testLength==0:
            testLength = int(line)
            strings = {}
            readstrings = 0
        else:
            line = line.strip()
            if line in strings:
                strings[line]+=1
            else:
                strings[line] = 1
            readstrings += 1
            if readstrings==testLength:
                doTest(testLength,strings)
            #print(exploded)
                testLength = 0
                testsDone += 1


    if (nrTests==testsDone):
        break


