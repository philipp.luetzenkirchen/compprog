#!/usr/bin/env python3

from pickle import FALSE,TRUE
import random
import sys
import math


def doTest(l,c):
    #print(l,c)
    max = 0
    mix = 0
    for x in c:
        if x >= max:
            mix = max
            max = x
        elif x > mix:
            mix = x 
    if max - mix > 1:
        return False
    return True

import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        if testLength==0:
            testLength = int(line)
        else:
            exploded = [ int(x) for x in line.replace("\n","").split(" ")]
            #print(exploded)
            if doTest(testLength,exploded) == True:
                print("YES")
            else :
                print("NO")
            #print(exploded)
            testLength = 0
            testsDone += 1


    if (nrTests==testsDone):
        break


