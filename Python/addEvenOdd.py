#!/usr/bin/env python3


import random
import sys


def doTest(l,c):
    evenIndex = True
    odd = {}
    for i in c:
        par = i%2
        if evenIndex in odd:
            if not odd[evenIndex]==par:
                return False
        else:
            odd[evenIndex]=par
        evenIndex = not evenIndex
    return True
import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        if testLength==0:
            testLength = int(line)
        else:
            exploded = [ int(x) for x in line.replace("\n","").split(" ")]
            if doTest(testLength,exploded) == True:
                print("YES")
            else :
                print("NO")
            #print(exploded)
            testLength = 0
            testsDone += 1


    if (nrTests==testsDone):
        break


