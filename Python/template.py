#!/usr/bin/env python3


import random
import sys


def doTest(l,c):
    
    return False
import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        if testLength==0:
            testLength = int(line)
        else:
            exploded = [ int(x) for x in line.replace("\n","").split(" ")]
            if doTest(testLength,exploded) == True:
                print("YES")
            else :
                print("NO")
            #print(exploded)
            testLength = 0
            testsDone += 1


    if (nrTests==testsDone):
        break


