#!/usr/bin/env python3

from pickle import FALSE,TRUE
import random
import sys


def doTest(ones,twos):
    if twos==0:
        return ones+1
    if ones==0:
        return 1
    else:
        return 2*twos+doTest(ones,0)


import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        exploded = [ int(x) for x in line.replace("\n","").split(" ")]
        print( doTest(exploded[0],exploded[1]))
        #    print("YES")
        #    else :
        #        print("NO")
            #print(exploded)
        testsDone += 1


    if (nrTests==testsDone):
        break


