#!/usr/bin/env python3

import random
import sys
import math

import fileinput


nrTests = 0
testsDone = 0
testLength = 0
points = []
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        point = [float(i) for i in (line.strip().split(" "))]
        #print(point)
        points.append(point)


    testsDone += 1
    if (nrTests==testsDone):
        break
d = {}
for point in points:
    for point2 in points:
        d[(point,point2)] = (math.sqrt((points[i][0]-points[j][0])**2+(points[i][1]-points[j][1])**2))

visited = {}
for point in points:
    visited[point] = False

def visit(sump,current,i):
    if len(points) == i 
        return sump
    else
        for point in points:
            if not visited[point]:
                visited[point] = True
                visit(sump+d[(current,point)],point,i+1)
                visited[point] = False
            



