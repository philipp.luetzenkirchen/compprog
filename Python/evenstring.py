#!/usr/bin/env python3

from pickle import FALSE,TRUE
import random
import sys


def doTask(string):
    #print(string)
    if len(string)==0:
        return 0
    for i in range(len(string)-1):
        if string[i]==string[i+1]:
            return doTask(string[:i]+string[(i+2):])
    for i in range(len(string)):
        wo = string[:i]+string[(i+1):]
        if wo.find("%s"%string[i]) < 0:
            return 1 + doTask(wo)
    return 1 +doTask(string[1:])


import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        print(doTask(line.replace("\n","")))
        testsDone += 1


    if (nrTests==testsDone):
        break


