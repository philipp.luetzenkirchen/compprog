#!/usr/bin/env python3


import random
import sys
import copy

def doTest(l,c):
    
    return False

import fileinput
nrTests = 0
testsDone = 0
testLength = 0
listOfInts = []
for line in fileinput.input():
    listOfInts.append(int(line))

n = listOfInts[0]
m = listOfInts[1]
numbers = listOfInts[2:(n+2)]
sums = listOfInts[(n+2):]

#print(n,m,numbers,sums)

assert len(sums) == m
assert len(numbers) == n

reachable = {}
reachable[0] = True
for i in numbers:
    reachable2 = copy.copy(reachable)
    for x in reachable2:
        reachable[x+i] = True
    #print(reachable)

for j in sums:
    if j in reachable:
        print("YES")
    else:
        print("NO")







