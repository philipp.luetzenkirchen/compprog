#!/usr/bin/env python3


import random
import sys


def doTest(l,c):
    state = "n"
    for cha in c:
        #print(cha, state)
        if state=="n" and cha == "R":
            state = "R"
        elif state=="n" and cha == "B":
            state = "B"

        elif state=="R" and cha == "B":
            state = "S"
        elif state=="B" and cha == "R":
            state = "S"
        
        elif state=="R" and cha == "W":
            return False
        elif state=="B" and cha == "W":
            return False
        
        elif state=="S" and cha == "W":
            state = "n"

        

    return (state=="n" or state == "S")
import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        if testLength==0:
            testLength = int(line)
        else:
            if doTest(testLength,line) == True:
                print("YES")
            else :
                print("NO")
            #print(exploded)
            testLength = 0
            testsDone += 1


    if (nrTests==testsDone):
        break


