#!/usr/bin/env python3

from curses import KEY_MAX
from pickle import FALSE,TRUE
import random
import sys


def doTest(l,c):
    
    return FALSE

import fileinput
testsDone = 0
hashmap = {}
nrTests = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line.strip())
    else:
        line = line.strip()
        if line in hashmap:
            hashmap[line] += 1
        else:
            hashmap[line] = 1

        testsDone += 1
    #print("'%s'"%line)
    if (nrTests==testsDone):
        break

keymax = ""
valmax = 0
for key in hashmap:
    #print("%s : %d"%(key,hashmap[key]))
    if hashmap[key] > valmax:
        keymax = key
        valmax = hashmap[key]
print(keymax)
