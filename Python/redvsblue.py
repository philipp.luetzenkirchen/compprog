#!/usr/bin/env python3


import random
import sys
import math


def doTest(l,c):
    
    return False

def RBString(r,b):
    if b==-1:
        return ""
    ratio = math.floor(r/(b+1))
    ret = ""
    for i in range(ratio):
        ret += "R"
    if b > 0:
        ret += "B"
    
    return ret + RBString(r-ratio,b-1)



import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        exploded = [ int(x) for x in line.replace("\n","").split(" ")]
        r = exploded[1]
        b = exploded[2]
        m = exploded[0]

        assert(r+b == m)

        print(RBString(r,b))


        testsDone+=1
    if (nrTests==testsDone):
        break


