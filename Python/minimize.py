#!/usr/bin/env python3

import math
import random
import sys

def f(a,b,x):
    result = 0
    for i in a:
        result += abs(x-i)
    
    for j in b:
        result += (x-j)**2
    return result
#def derivative(a,b,x):
#    result = -2*b-x


import fileinput

a=[]
b=[]
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        if len(a) < nrTests:
            a.append(float(line))
        else:
            b.append(float(line))

sumb = sum(b)
a = sorted(a)
#print(a,b)

minvalue = f(a,b,(2*sumb-nrTests)/(2*nrTests))
minarument = (2*sumb-nrTests)/(2*nrTests)

for i in range(len(a)):
    res = (2*sumb+nrTests-2*i)/(2*nrTests)
    val = f(a,b,res)
    if val < minvalue:
        minvalue = val
        minarument = res

    #print(val,res)
    res = a[i]
    val = f(a,b,res)
    if val < minvalue:
        minvalue = val
        minarument = res
    #print(minvalue,minarument)


print(minarument)

#print(f(a,b,1.9419183433333334),f(a,b,1.99999997))

