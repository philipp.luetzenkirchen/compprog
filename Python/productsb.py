#!/usr/bin/env python3

from pickle import FALSE,TRUE
import random
import sys
import math



def doTest(l,c):
    pivot = math.floor(l/2)
    tail = l - pivot
    prod = 1
    cutheadmax = pivot
    cutheadmin = pivot
    maxprodhead = prod
    minprodhead = prod
    for i in range(pivot,0,-1):
        prod *= c[i]
        if prod > maxprodhead:
            maxprodhead = prod
            cutheadmax = i
        if prod < minprodhead:
            minprodhead = prod
            cutheadmin = i

    prod = 1
    cuttailmax = tail
    cuttailmin = tail
    cut = tail
    maxprodtail = prod
    minprodtail = prod
    for i in range(pivot+1,l):
        cut -= 1
        prod *= c[i]
        if prod > maxprodtail:
            maxprodhead = prod
            cuttailmax = cut
        if prod < minprodtail:
            minprodtail = prod
            cuttailmin = cut
    res = []
    res.append((maxprodtail,cutheadmax,cuttailmax))  


import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        if testLength==0:
            testLength = int(line)
        else:
            exploded = [ int(x) for x in line.replace("\n","").split(" ")]
            print( doTest(testLength,exploded))

            #print(exploded)
            testsDone += 1


    if (nrTests==testsDone):
        break


