#!/usr/bin/env python3


import random
import sys
import fileinput

def doTest(n,k,bits):
    #assert n == len(bits)
    choices = {}
    m = k
    if k%2==1:
        bits = [not x for x in bits]
    for i in range(n):
        if not bits[i] and m >0 :
            choices[i] = 1
            bits[i] = not bits[i]
            m -= 1
        else:
            choices[i] = 0
    if m%2==1:
        bits[n-1] = not bits[n-1] 
    choices[n-1]+=m

    #assert sum([choices[i] for i in range(n)]) == k
    firstline = ""
    for bit in bits:
        if bit:
            firstline += "1"
        else:
            firstline += "0"
    secondline = ""
    for i in range(n):
        secondline += "%d "%choices[i]

    print(firstline)
    print(secondline)

    return

nrTests = 0
testsDone = 0
testLength = 0
intratestIndex = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        if intratestIndex == 0:
            intratestIndex = 1
            exploded = [ int(x) for x in line.replace("\n","").split(" ")]
            n = exploded[0]
            k = exploded[1]
        else:
            #print(exploded)
            intratestIndex = 0
            bitstring = []

            for cha in line:
                #print(cha,line)
                if cha=='0':
                    bitstring.append(False)
                elif cha=='1':
                    bitstring.append(True)
                else:
                    continue

            doTest(n,k,bitstring)


            testsDone += 1


    if (nrTests==testsDone):
        break


