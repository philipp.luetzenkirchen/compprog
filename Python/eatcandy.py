#!/usr/bin/env python3


import random
import sys


def doTest(l,c):
    #print(c)
    eaten = 0
    successfullEaten = 0
    alice = 0
    aliceNext = 0
    bob = 0
    bobNext = l-1
    while eaten <= l:
        #print(alice,bob)
        if alice == bob:
            successfullEaten = eaten
        if alice >= bob:
            eaten+=1
            bob += c[bobNext]
            bobNext -= 1
        else:
            eaten +=1
            alice += c[aliceNext]
            aliceNext += 1
    print(successfullEaten)

import fileinput
nrTests = 0
testsDone = 0
testLength = 0
for line in fileinput.input():
    if nrTests==0:
        nrTests = int(line)
    else:
        if testLength==0:
            testLength = int(line)
        else:
            exploded = [ int(x) for x in line.replace("\n","").split(" ")]
            doTest(testLength,exploded)
            #print(exploded)
            testLength = 0
            testsDone += 1


    if (nrTests==testsDone):
        break


