
#include <cmath>
#include <vector>
#include <set>
#include <cassert>
#include <iostream>
using namespace std;
typedef long long lint; 

std::set<int> primes = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113};
std::vector<int> given = {};
int k;
void printArray(vector<int> & array){
  for(unsigned int i = 0; i < array.size(); i++)cout <<" " <<  array[i];
  cout << endl;
}

bool checkuntil(vector<int>& square,int until);

void doSwap(vector<int> & array, int a , int b){
  int h = array[a];
  array[a] = array[b];
  array[b] = h;  
}
bool permute(vector<int> & array, int depth){
  if(depth==1){
    //printArray(array);
    //cout << depth << endl;
    return checkuntil(array,array.size());
  }
  for(int i = 0; i < depth;i++){
    doSwap(array,array.size()-depth,array.size()-depth+i);
    if(checkuntil(array,((k*k)-(depth)))){
      if(permute(array,depth-1)){
        return true;
      }
    }
        doSwap(array,array.size()-depth,array.size()-depth+i);
    //doSwap(array,depth-1,depth-1-i);
  }
  return false;
}

bool checkuntil(vector<int>& square,int until){
  for(int i = 0; i < until;i++){
    if(given[i] && given[i]!= square[i]){
      
      cout << "checkuntil " << until << ", failed at given " << given[i] << endl;
      return false;
    }
  }
  for(int i = 0; (i+1)*k<until;i++){
    int sum = 0;
    for(int j = 0; j<k;j++){
      sum += square[i*k+j];
    }
    if (primes.count(sum)==0){
      cout << "checkuntil " << until << ", failed at row " << i << endl;
      return false;
    }
  }
  for(int i = 0; i < until-(k*(k-1)); i++){
    int sum = 0;
    for(int j = 0; j<k;j++){
      sum += square[j*k+i];
    }
    if (primes.count(sum)==0){
      cout << "checkuntil " << until << ", failed at col " << i << endl;
      return false;
    }
  }
  return true;
}

int main() {
  cin >> k;
  for (int i = 0; i < k*k; i++){
    int j;
    cin >> j;
    given.push_back(j);
  }
  /*cout << "given" << endl;
  for(int i = 0; i < (k*k); i++){
    cout << given[i] << ";";
  }
  cout << endl;*/

  vector<int> permutation(k*k);

  for(int i = 0; i < (k*k); i++){
    //permutation.push_back(i);
    permutation[i] = i+1;
  }
  
  permute(permutation,(k*k));

  for(int i = 0; i < k; i++){
    cout << endl;
    for(int j = 0; j < k; j++){

      cout << " " << permutation[i*k+j];
    }
  }
  return 0;
}

