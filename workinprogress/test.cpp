
#include <cmath>
#include <vector>
#include <cassert>
#include <iostream>
using namespace std;
typedef long long lint; 

void printArray(vector<int> & array){
  for(unsigned int i = 0; i < array.size(); i++)cout <<" " <<  array[i];
  cout << endl;
}
void doSwap(vector<int> & array, int a , int b){
  int h = array[a];
  array[a] = array[b];
  array[b] = h;  
}
void permute(vector<int> & array, int depth){
  if(depth==1){
    printArray(array);
    return;
  }
  for(int i = 0; i < depth;i++){
    doSwap(array,depth-1,depth-1-i);
    permute(array,depth-1);
    doSwap(array,depth-1,depth-1-i);
  }
}

int main() {
  /*int k,m;
  cin >> k;
  cin >> m;
  vector<int> l; 
  for (int i = 0; i < k; i++){
    int j;
    cin >> j;
    l.push_back(j);
  }*/
  vector<int> permutation = {0,1,2,3} ;
  for(unsigned int i = 0; i < sizeof(permutation)/4; i++)cout <<" " <<  permutation[i];
  cout << endl;
  
  permute(permutation,4);

  cout << "Hello world!" << endl;

  return 0;
}

